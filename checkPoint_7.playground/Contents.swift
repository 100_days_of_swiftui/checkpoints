/*
 
 Checkpoint - 7 of #100DaysofSwiftUI

 - Make a class hierarchy for animals, starting with Animal at the top.
 - Then Dog and Cat as subclasses, then Corgi and Poodle as subclasses of Dog dnd Persian and Lion as subclasses of Cat.
 - The Animal class should have a legs integer property that tracks how many legs the animal has.
 - The Dog class should have a speak() method that prints a generic dog barking string, but each of the subclasses should print something slightly different.
 - The Cat class should have a matching speak() method, again with each subclass printing something different.
 - The Cat class should have an isTame Boolean property, provided using an initializer.
*/

// MARK: - Solution

class Animal {
    let legs: Int
    
    init(legs: Int) {
        self.legs = legs
    }
}

class Dog: Animal {
    override init(legs: Int) {
        super.init(legs: 4)
    }
    
    func speak() {
        print("Woof.. Woof..")
    }
}

final class Corgi: Dog {
    override func speak() {
        print("Arf.. Arf..")
    }
}

final class Poodle: Dog {
    override func speak() {
        print("Yap.. Yap..")
    }
}

class Cat: Animal {
    let isTame: Bool
    
    init(isTame: Bool) {
        self.isTame = isTame
        super.init(legs: 4)
    }
    
    func speak() {
        print("Meow..")
    }
}

final class Persian: Cat {
    override func speak() {
        self.isTame ? print("Mew.. Mew..") : print("Purr.. Purr..")
    }
}

final class Lion: Cat {
    override func speak() {
        self.isTame ? print("Rrroarrr.. Rrroarrr..") : print("Roar.. Roar")
    }
}


let poodleDog = Poodle(legs: 4)
poodleDog.speak()

let dog = Dog(legs: 4)
dog.speak()

let lion = Lion(isTame: false)
lion.speak()

let persianCat = Persian(isTame: true)
persianCat.speak()
