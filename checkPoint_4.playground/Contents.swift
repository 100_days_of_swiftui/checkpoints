/*
 
 Checkpoint - 4 of #100DaysofSwiftUI
 
- Write a function that accepts an integer from 1 through 10,000 and returns the integer square root of that number.
 
 - You can’t use Swift’s built-in sqrt() function or similar.
 – You need to find the square root yourself.
 - If the number is less than 1 or greater than 10,000 you should throw an “out of bounds” error.
 - You should only consider integer square roots – don’t worry about the square root of 3 being 1.732, for example.
 - If you can’t find the square root, throw a “no root” error.
 
*/

// MARK: - Solution

enum SquareRootError: String, Error {
    case noRoot = "no root"
    case outOfBounds = "out of bounds"
}

func squareRoot(of number: Int) throws -> Int {
    if number < 1 || number > 10000 {
        throw SquareRootError.outOfBounds
    }
    
    for i in 1...100 where number == i * i {
            return i
    }
    throw SquareRootError.noRoot
}

do {
    print(try squareRoot(of: 144), terminator: "")
} catch {
    if let error = error as? SquareRootError {
        print(error.rawValue, terminator: "")
    }
}
