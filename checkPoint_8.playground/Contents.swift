/*
 
 Checkpoint - 8 of #100DaysofSwiftUI
 
 - Make a protocol that describes a building, adding various properties and methods,
 - Create two structs, House and Office, that conform to it,
 - A property storing how many rooms it has.
 - A property storing the cost as an integer (e.g. 500,000 for a building costing $500,000.)
 - A property storing the name of the estate agent responsible for selling the building.
 - A method for printing the sales summary of the building, describing what it is along with its other properties.
 
*/

// MARK: - Solution

protocol Building {
    var rooms: Int {get set}
    var cost: Int {get set}
    var agent: String {get set}
    var salesSummary: String {get set}
    func displaySalesSummary()
}

extension Building {
    func displaySalesSummary() {
        if self.salesSummary.isEmpty {
            print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            print("Agent \(self.agent)'s Building wasn't sold yet")
            print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            print("\n")
        } else {
            print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            print("Sales summary of Agent \(self.agent)'s Building")
            print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            print("No. of Rooms: \(self.rooms)")
            print("Building Cost: \(self.cost)")
            print("Summary of the Sales: \(self.salesSummary)")
            print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            print("\n")
        }
    }
}

struct House: Building {
    var rooms: Int
    var cost: Int
    var agent: String
    var salesSummary: String
    
    init(
        rooms: Int,
        cost: Int,
        agent: String,
        salesSummary: String
    ) {
        self.rooms = rooms
        self.cost = cost
        self.agent = agent
        self.salesSummary = salesSummary
    }
}

struct Office: Building {
    var rooms: Int
    var cost: Int
    var agent: String
    var salesSummary: String
    
    init(
        rooms: Int,
        cost: Int,
        agent: String,
        salesSummary: String
    ) {
        self.rooms = rooms
        self.cost = cost
        self.agent = agent
        self.salesSummary = salesSummary
    }
}

let shyamHouse = House(rooms: 3, cost: 2000000, agent: "John", salesSummary: "")
shyamHouse.displaySalesSummary()

let excelOffice = Office(
    rooms: 7,
    cost: 6000000,
    agent: "Lee",
    salesSummary: """
        \n
        Excel Office was sold to Mr.Jeff,
        At Govt. Registration office,
        On 25th April 2024.
    """
)
excelOffice.displaySalesSummary()

