/*
 
 Checkpoint - 9 of #100DaysofSwiftUI
 
 - Write a function that accepts an optional array of integers, and returns one randomly. 
 - If the array is missing or empty, return a random number in the range 1 through 100.
 - I want you to write your function in a single line of code.
 
*/

// MARK: - Solution

func getRandomInteger(from array: [Int]?) -> Int { array?.randomElement() ?? Int.random(in: 1...100) }

print("Random Integer: \(getRandomInteger(from: []))")
print("Random Integer: \(getRandomInteger(from: nil))")
print("Random Integer: \(getRandomInteger(from: [12, 34, 56, 78, 90, 09, 87, 65, 43, 21]))")
