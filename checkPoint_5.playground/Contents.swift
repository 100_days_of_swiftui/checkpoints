/*
 
 Checkpoint - 5 of #100DaysofSwiftUI
 
 - let luckyNumbers = [7, 4, 38, 21, 16, 15, 12, 33, 31, 49]
 - Your job is to:
    - Filter out any numbers that are even
    - Sort the array in ascending order
    - Map them to strings in the format “7 is a lucky number”
    - Print the resulting array, one item per line
*/

// MARK: - Solution

let luckyNumbers = [7, 4, 38, 21, 16, 15, 12, 33, 31, 49]
let filterEvenNumbers = { (numbers: [Int]) -> [Int] in
    return numbers.filter({ !$0.isMultiple(of: 2)})
}
let ascendingOrderSortedNumbers = { (filteredNumbers: [Int]) -> [Int] in
    return filteredNumbers.sorted()
}
let mapAndPrintResult = { (sortedNumbers: [Int]) in
    sortedNumbers.forEach { number in
        print(" \(number) is a lucky number")
    }
}

func generateResult(for input: [Int]) {
    let filteredNumbers = filterEvenNumbers(input)
    let sortedNumbers = ascendingOrderSortedNumbers(filteredNumbers)
    mapAndPrintResult(sortedNumbers)
}
generateResult(for: luckyNumbers)
