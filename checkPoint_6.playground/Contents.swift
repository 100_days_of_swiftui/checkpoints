/*
 
 Checkpoint - 6 of #100DaysofSwiftUI
 
 - Create a struct to store information about a car, including its model, number of seats, and current gear.
 - Then add a method to change gears up or down.
 - Have a think about variables and access control.
 - What data should be a variable rather than a constant.
 - What data should be exposed publicly?
 - Gear-changing method validate its input, from 1 to 10.

*/

// MARK: - Solution

struct CarInfo {
    enum GearDirection {
        case upward
        case downward
    }
    
    let model: String
    let numberOfSeats: Int
    private(set) var currentGear: Int = 1
    
    init(
        model: String,
        numberOfSeats: Int
    ) {
        self.model = model
        self.numberOfSeats = numberOfSeats
    }
    
    mutating func changeGear(to direction: GearDirection) {
        switch direction {
        case .upward:
            if self.currentGear == 10 {
                print("Oops! Reached Maximum, You're not allowed to change the gear")
                break
            }
            self.currentGear += 1
            print("Gear changed to \(self.currentGear)")
        case .downward:
            if self.currentGear == 1 {
                print("Oops! Reached Minimum, You're not allowed to change the gear")
                break
            }
            self.currentGear -= 1
            print("Gear changed to \(self.currentGear)")
        }
    }
}

var newCar = CarInfo(model: "Volvo S90", numberOfSeats: 5)
newCar.changeGear(to: .upward)
newCar.changeGear(to: .upward)
newCar.changeGear(to: .upward)
newCar.changeGear(to: .downward)
print(newCar)
