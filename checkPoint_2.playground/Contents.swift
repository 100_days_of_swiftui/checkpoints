/*
 
 Checkpoint - 2 of #100DaysofSwiftUI
 
 - To create an array of strings,
 - Write some code that prints the number of items in the array,
 - And also the number of unique items in the array.
 
*/

// MARK: - Solution

let arrayOfColours = ["red" , "blue" , "cyan" , "mint" , "red" , "gray" , "blue", "pink", "mint", "cyan"]
let uniqueColours: Int = Set(arrayOfColours).count

print("Number of colours in the array is: \(arrayOfColours.count)", terminator: "")

print("Number of unique colours in the array is: \(uniqueColours)",terminator: "")

