/*
 
Checkpoint - 1 of #100DaysofSwiftUI

 - Creates a constant holding any temperature in Celsius.
 - Converts it to Fahrenheit by multiplying by 9, dividing by 5, then adding 32.
 - Prints the result for the user, showing both the Celsius and Fahrenheit values.
 
*/

// MARK: - Solution

let celsius: Double = 27.3
let fahrenheit = celsius * 9 / 5 + 32
print("Current Tempature is \(celsius)°C / \(fahrenheit)°F" , terminator: "")

